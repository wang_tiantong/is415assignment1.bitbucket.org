var baseLayer = new ol.layer.Group({'title': 'Base maps',layers: [new ol.layer.Tile({title: 'OSM',
                                              source: new ol.source.OSM()
                                            })]});
var format_CostalOutline = new ol.format.GeoJSON();
var features_CostalOutline = format_CostalOutline.readFeatures(geojson_CostalOutline, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_CostalOutline = new ol.source.Vector();
jsonSource_CostalOutline.addFeatures(features_CostalOutline);var lyr_CostalOutline = new ol.layer.Vector({
                source:jsonSource_CostalOutline, 
                style: style_CostalOutline,
                title: "CostalOutline"
            });var format_DGPSubZone = new ol.format.GeoJSON();
var features_DGPSubZone = format_DGPSubZone.readFeatures(geojson_DGPSubZone, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_DGPSubZone = new ol.source.Vector();
jsonSource_DGPSubZone.addFeatures(features_DGPSubZone);var lyr_DGPSubZone = new ol.layer.Vector({
                source:jsonSource_DGPSubZone, 
                style: style_DGPSubZone,
                title: "DGPSubZone"
            });var format_natural = new ol.format.GeoJSON();
var features_natural = format_natural.readFeatures(geojson_natural, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_natural = new ol.source.Vector();
jsonSource_natural.addFeatures(features_natural);var lyr_natural = new ol.layer.Vector({
                source:jsonSource_natural, 
                style: style_natural,
                title: "natural"
            });var format_waterways = new ol.format.GeoJSON();
var features_waterways = format_waterways.readFeatures(geojson_waterways, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_waterways = new ol.source.Vector();
jsonSource_waterways.addFeatures(features_waterways);var lyr_waterways = new ol.layer.Vector({
                source:jsonSource_waterways, 
                style: style_waterways,
                title: "waterways"
            });var format_polyclinic = new ol.format.GeoJSON();
var features_polyclinic = format_polyclinic.readFeatures(geojson_polyclinic, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_polyclinic = new ol.source.Vector();
jsonSource_polyclinic.addFeatures(features_polyclinic);var lyr_polyclinic = new ol.layer.Vector({
                source:jsonSource_polyclinic, 
                style: style_polyclinic,
                title: "polyclinic"
            });var format_denguepatient5bufferdissolve = new ol.format.GeoJSON();
var features_denguepatient5bufferdissolve = format_denguepatient5bufferdissolve.readFeatures(geojson_denguepatient5bufferdissolve, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_denguepatient5bufferdissolve = new ol.source.Vector();
jsonSource_denguepatient5bufferdissolve.addFeatures(features_denguepatient5bufferdissolve);var lyr_denguepatient5bufferdissolve = new ol.layer.Vector({
                source:jsonSource_denguepatient5bufferdissolve, 
                style: style_denguepatient5bufferdissolve,
                title: "dengue_patient_500buffer_dissolve"
            });var format_denguepatientsxyprocessed = new ol.format.GeoJSON();
var features_denguepatientsxyprocessed = format_denguepatientsxyprocessed.readFeatures(geojson_denguepatientsxyprocessed, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_denguepatientsxyprocessed = new ol.source.Vector();
jsonSource_denguepatientsxyprocessed.addFeatures(features_denguepatientsxyprocessed);var lyr_denguepatientsxyprocessed = new ol.layer.Vector({
                source:jsonSource_denguepatientsxyprocessed, 
                style: style_denguepatientsxyprocessed,
                title: "dengue_patients_xy_processed"
            });var format_denguexyprocessed = new ol.format.GeoJSON();
var features_denguexyprocessed = format_denguexyprocessed.readFeatures(geojson_denguexyprocessed, 
            {dataProjection: 'EPSG:4326', featureProjection: 'EPSG:3857'});
var jsonSource_denguexyprocessed = new ol.source.Vector();
jsonSource_denguexyprocessed.addFeatures(features_denguexyprocessed);cluster_denguexyprocessed = new ol.source.Cluster({
  distance: 10,
  source: jsonSource_denguexyprocessed
});var lyr_denguexyprocessed = new ol.layer.Vector({
                source:cluster_denguexyprocessed, 
                style: style_denguexyprocessed,
                title: "dengue_xy_processed"
            });

lyr_CostalOutline.setVisible(false);lyr_DGPSubZone.setVisible(false);lyr_natural.setVisible(false);lyr_waterways.setVisible(false);lyr_polyclinic.setVisible(false);lyr_denguepatient5bufferdissolve.setVisible(false);lyr_denguepatientsxyprocessed.setVisible(false);lyr_denguexyprocessed.setVisible(false);
var layersList = [baseLayer,lyr_CostalOutline,lyr_DGPSubZone,lyr_natural,lyr_waterways,lyr_polyclinic,lyr_denguepatient5bufferdissolve,lyr_denguepatientsxyprocessed,lyr_denguexyprocessed];
