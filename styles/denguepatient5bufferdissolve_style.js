var size = 0;

var styleCache_denguepatient5bufferdissolve={}
var style_denguepatient5bufferdissolve = function(feature, resolution){
    var value = ""
    var size = 0;
    var style = [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: "rgba(0,0,0,0.568627)", lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}) , fill: new ol.style.Fill({color: "rgba(255,127,0,0.568627)"})
    })];
    if ("") {
        var labelText = "";
    } else {
        var labelText = ""
    }
    var key = value + "_" + labelText

    if (!styleCache_denguepatient5bufferdissolve[key]){
        var text = new ol.style.Text({
              font: '10.725px Calibri,sans-serif',
              text: labelText,
              textBaseline: "center",
              textAlign: "left",
              offsetX: 5,
              offsetY: 3,
              fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
              }),
            });
        styleCache_denguepatient5bufferdissolve[key] = new ol.style.Style({"text": text})
    }
    var allStyles = [styleCache_denguepatient5bufferdissolve[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};