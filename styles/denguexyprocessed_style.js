var size = 0;

var styleCache_denguexyprocessed={}
var style_denguexyprocessed = function(feature, resolution){
    var value = ""
    var size = feature.get('features').length;
    var style = [ new ol.style.Style({
        image: new ol.style.Circle({radius: 2.2 + size,
            stroke: new ol.style.Stroke({color: 'rgba(0,0,0,255)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 1}), fill: new ol.style.Fill({color: "rgba(227,26,28,1.0)"})})
    })];
    if ("") {
        var labelText = "";
    } else {
        var labelText = ""
    }
    var key = value + "_" + labelText

    if (!styleCache_denguexyprocessed[key]){
        var text = new ol.style.Text({
              font: '10.725px Calibri,sans-serif',
              text: labelText,
              textBaseline: "center",
              textAlign: "left",
              offsetX: 5,
              offsetY: 3,
              fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
              }),
            });
        styleCache_denguexyprocessed[key] = new ol.style.Style({"text": text})
    }
    var allStyles = [styleCache_denguexyprocessed[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};