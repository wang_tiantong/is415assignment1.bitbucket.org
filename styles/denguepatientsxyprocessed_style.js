var size = 0;
var ranges_denguepatientsxyprocessed = [[0.000000, 19.600000, [ new ol.style.Style({
        image: new ol.style.Circle({radius: 2.0 + size,
            stroke: new ol.style.Stroke({color: 'rgba(0,0,0,255)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 1}), fill: new ol.style.Fill({color: "rgba(254,240,217,1.0)"})})
    })]],
[19.600000, 39.200000, [ new ol.style.Style({
        image: new ol.style.Circle({radius: 2.0 + size,
            stroke: new ol.style.Stroke({color: 'rgba(0,0,0,255)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 1}), fill: new ol.style.Fill({color: "rgba(253,204,138,1.0)"})})
    })]],
[39.200000, 58.800000, [ new ol.style.Style({
        image: new ol.style.Circle({radius: 2.0 + size,
            stroke: new ol.style.Stroke({color: 'rgba(0,0,0,255)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 1}), fill: new ol.style.Fill({color: "rgba(252,141,89,1.0)"})})
    })]],
[58.800000, 78.400000, [ new ol.style.Style({
        image: new ol.style.Circle({radius: 2.0 + size,
            stroke: new ol.style.Stroke({color: 'rgba(0,0,0,255)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 1}), fill: new ol.style.Fill({color: "rgba(227,74,51,1.0)"})})
    })]],
[78.400000, 98.000000, [ new ol.style.Style({
        image: new ol.style.Circle({radius: 2.0 + size,
            stroke: new ol.style.Stroke({color: 'rgba(0,0,0,255)', lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 1}), fill: new ol.style.Fill({color: "rgba(179,0,0,1.0)"})})
    })]]];
var styleCache_denguepatientsxyprocessed={}
var style_denguepatientsxyprocessed = function(feature, resolution){
    var value = feature.get("AGE");
    var style = ranges_denguepatientsxyprocessed[0][2];
    for (i = 0; i < ranges_denguepatientsxyprocessed.length; i++){
        var range = ranges_denguepatientsxyprocessed[i];
        if (value > range[0] && value<=range[1]){
            style =  range[2];
        }
    };
    if ("") {
        var labelText = "";
    } else {
        var labelText = ""
    }
    var key = value + "_" + labelText

    if (!styleCache_denguepatientsxyprocessed[key]){
        var text = new ol.style.Text({
              font: '10.725px Calibri,sans-serif',
              text: labelText,
              textBaseline: "center",
              textAlign: "left",
              offsetX: 5,
              offsetY: 3,
              fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
              }),
            });
        styleCache_denguepatientsxyprocessed[key] = new ol.style.Style({"text": text})
    }
    var allStyles = [styleCache_denguepatientsxyprocessed[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};