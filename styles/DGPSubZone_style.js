var size = 0;
var ranges_DGPSubZone = [[0.000000, 2690.000000, [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}) , fill: new ol.style.Fill({color: "rgba(247,251,255,1.0)"})
    })]],
[2690.000000, 7339.000000, [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}) , fill: new ol.style.Fill({color: "rgba(199,220,239,1.0)"})
    })]],
[7339.000000, 16217.000000, [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}) , fill: new ol.style.Fill({color: "rgba(114,178,215,1.0)"})
    })]],
[16217.000000, 28578.000000, [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}) , fill: new ol.style.Fill({color: "rgba(40,120,184,1.0)"})
    })]],
[28578.000000, 44626.000000, [ new ol.style.Style({
        stroke: new ol.style.Stroke({color: "rgba(0,0,0,1.0)", lineDash: null, lineCap: 'butt', lineJoin: 'miter', width: 0}) , fill: new ol.style.Fill({color: "rgba(8,48,107,1.0)"})
    })]]];
var styleCache_DGPSubZone={}
var style_DGPSubZone = function(feature, resolution){
    var value = feature.get("Census2000_new_AGE20_39");
    var style = ranges_DGPSubZone[0][2];
    for (i = 0; i < ranges_DGPSubZone.length; i++){
        var range = ranges_DGPSubZone[i];
        if (value > range[0] && value<=range[1]){
            style =  range[2];
        }
    };
    if ("") {
        var labelText = "";
    } else {
        var labelText = ""
    }
    var key = value + "_" + labelText

    if (!styleCache_DGPSubZone[key]){
        var text = new ol.style.Text({
              font: '10.725px Calibri,sans-serif',
              text: labelText,
              textBaseline: "center",
              textAlign: "left",
              offsetX: 5,
              offsetY: 3,
              fill: new ol.style.Fill({
                color: "rgba(0, 0, 0, 255)"
              }),
            });
        styleCache_DGPSubZone[key] = new ol.style.Style({"text": text})
    }
    var allStyles = [styleCache_DGPSubZone[key]];
    allStyles.push.apply(allStyles, style);
    return allStyles;
};